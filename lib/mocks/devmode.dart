import 'package:flutter/material.dart';

class DevModeScreen extends StatefulWidget {
  @override
  _DevModeScreenState createState() => _DevModeScreenState();
}

class _DevModeScreenState extends State<DevModeScreen> {
  bool isAddData;
  @override
  void initState() {
    isAddData = false;

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
         title: Text("DEVMODE")
        ),
        body: SafeArea(
          child: Container(
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      'Add Data',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                    Checkbox(
                      value: isAddData, 
                      onChanged: (bool value) {
                        setState(() {
                          this.isAddData = !this.isAddData;
                        });
                      },
                    )
                  ]
                ),
                Text('hi')
                
              ],
            ),
          ),
          
        ),
      ),
    );
  }
}