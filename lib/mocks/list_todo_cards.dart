String mockListTodoCards = """

{
    "currentPage": 1,
    "totalItems" : 8,
    "items": [
        {
            "title" : "note 1",
            "description" : "mo ta 1",
            "status" : true
        },
        {
            "title" : "note 2",
            "description" : "mo ta 2",
            "status" : false
        },
        {
            "title" : "note 3",
            "description" : "mo ta 3",
            "status" : true
        }
       
    ]
}

""";