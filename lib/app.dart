import 'package:flutter/material.dart';
import 'package:flutter_practical_todo/app/bloc/bloc_app.dart';
import 'package:flutter_practical_todo/app/screens/dashboard/dashboard.dart';
import 'package:flutter_practical_todo/app/values/colors.dart';

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        brightness: Brightness.light,
        primarySwatch: AppColor.primaryColor,
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.red,
        buttonColor: Colors.red,
      ),
      home: Dashboard(blocApp: BlocApp())
    );
  }
}