import 'dart:convert';

import 'package:flutter_practical_todo/app/models/list_todo_card.dart';
import 'package:flutter_practical_todo/app/models/todo_card.dart';
import 'package:flutter_practical_todo/app/repository/rest_mock.dart';
import 'package:flutter_practical_todo/app/utils.dart';
import 'package:flutter_practical_todo/base/bloc_base.dart';
import 'package:flutter_practical_todo/base/user_default.dart';
import 'package:flutter_practical_todo/mocks/list_todo_cards.dart';

class BlocApp extends BlocBase{
  @override
  dispose() {
    return null;
  }

  @override
  initialize() async{
    await UserDefault.getInstance().init();

    if(UserDefault.getInstance().getBoolean('first_launch') == null) {
      printLog("[BlocApp] first_launch ");

      // ListTodoCards listCards = ListTodoCards.fromJson(jsonDecode(mockListTodoCards));
      // listCards.items.add(
      //   TodoCard(
      //     title: 'hihi zx',
      //     description: 'haha aq',
      //     created: '11',
      //     updated: '22',
      //     status: true
      //   )
      // );

      // for(var i = 0; i < listCards.items.length; i ++) {
      //   await appApiService.client.addTodoCard(listCards.items[i].toJson());
      // }
     
      // await UserDefault.getInstance().setBoolean('first_launch', true);
      // printLog("[BlocApp] Data added ");

    }
    else {
      printLog("[BlocApp] Data loaded ");
    }
  }
}