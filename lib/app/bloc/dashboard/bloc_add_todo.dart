import 'package:flutter/material.dart';
import 'package:flutter_practical_todo/app/models/todo_card.dart';
import 'package:flutter_practical_todo/base/bloc_base.dart';

class BlocAddTodo extends BlocBase{
  final titleController = TextEditingController();
  final descriptionController = TextEditingController();

  @override
  dispose() {
    titleController.dispose();
    descriptionController.dispose();
  }

  @override
  initialize() {
  }

  bool addTodo() {
    if(titleController.text.isEmpty || descriptionController.text.isEmpty) {
      return false;
    }
        
    var todo = TodoCard(
      title: titleController.text,
      description: descriptionController.text,
      created: DateTime.now().toString(),
      updated: DateTime.now().toString(),
      status: false
    );

    appApiService.client.addTodoCard(todo.toJson());

    titleController.text = '';
    descriptionController.text = '';

    return true;

  }
}