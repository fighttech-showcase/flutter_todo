import 'package:flutter_practical_todo/app/models/list_todo_card.dart';
import 'package:flutter_practical_todo/app/models/todo_card.dart';
import 'package:flutter_practical_todo/app/utils.dart';
import 'package:flutter_practical_todo/base/bloc_base.dart';
import 'package:rxdart/rxdart.dart';

class BlocHome extends BlocBase {

  // ------------------------ STREAM ------------------------
  final _controllerHomeNews = BehaviorSubject<ListTodoCards>();
  Stream<ListTodoCards> get controllerListTodosCard => _controllerHomeNews.stream;
  ListTodoCards listCards;

  @override
  initialize() async{
    listCards = await appApiService.client.getListTodoCards();
    if(listCards != null) {
      _controllerHomeNews.sink.add(listCards);
      printLog("[BlocHome] ${listCards.toJson()}");
    }
  }

  @override
  dispose() {
    _controllerHomeNews.close();
  }

   Future<bool> onChaneItem(TodoCard card) async{
    card.status = ! card.status;
    printLog("[BlocHome] onChaneItem${card.toJson()}");

    return appApiService.client.updateTodo(card.toJson(), card.id);
  }

}