import 'package:flutter_practical_todo/app/models/todo_card.dart';
import 'package:flutter_practical_todo/base/bloc_base.dart';
import 'package:rxdart/rxdart.dart';

class BlocListTodo extends BlocBase {
  // ------------------------ STREAM ------------------------
  final _controllerHomeNews = BehaviorSubject<List<TodoCard>>();
  Stream<List<TodoCard>> get controllerListTodosCard => _controllerHomeNews.stream;
  
  @override
  dispose() {
    _controllerHomeNews.close();
  }

  @override
  initialize({ bool status}) async{
    var listCards = await appApiService.client.getTodoByStatus(status);
    _controllerHomeNews.sink.add(listCards);
  } 

}