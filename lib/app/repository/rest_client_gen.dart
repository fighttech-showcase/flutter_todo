import 'package:flutter_practical_todo/app/models/list_todo_card.dart';
import 'package:flutter_practical_todo/app/models/todo_card.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'rest_client_gen.g.dart';

/// https://pub.dev/packages/retrofit

@RestApi(baseUrl: "")
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  // ----------- T -----------
  @GET("countTodo")
  Future<ListTodoCards> getListTodoCards();
  @GET("getTodoByStatus/{id}")
  Future<List<TodoCard>> getTodoByStatus(@Path() bool status);
  @GET("todo/{id}/{status}")
  Future<bool> updateTodo(@Body() Map<String, dynamic> todoCard, @Path() int id);

  @GET("add")
  Future<bool> addTodoCard(@Body() Map<String, dynamic> todoCard);

}
