import 'package:flutter_practical_todo/app/models/list_todo_card.dart';
import 'package:flutter_practical_todo/app/models/todo_card.dart';
import 'package:flutter_practical_todo/app/modules/sqflite/db.dart';
import 'package:flutter_practical_todo/app/repository/rest_client_gen.dart';
import 'package:flutter_practical_todo/app/utils.dart';

class RestClientLocal implements RestClient{
  @override
  Future<ListTodoCards> getListTodoCards() async{
    // return Future.value(ListTodoCards.fromJson(jsonDecode(mockListTodoCards)));

   var maps = await DB.sharedInstance.queryAllRows();

    printLog(maps.toString());

    var list = List<TodoCard>.generate(maps.length, (i) {
      return TodoCard(
        id:        maps[i]['id'],
        title:        maps[i]['title'],
        description:  maps[i]['description'],
        created:      maps[i]['created'],
        updated:      maps[i]['updated'],
        status:       maps[i]['status'] == 1 ? true : false,
      );
    });

    return Future.value(ListTodoCards(
      currentPage: 1,
      totalItems: 3,
      items: list
    ));
  }

  @override
  Future<bool> addTodoCard(Map<String, dynamic> todoCard) {
    try{
      DB.sharedInstance.createTodo(TodoCard.fromJson(todoCard));
      return Future.value(true);
    }
    catch(err) {
      return Future.value(false);
    }
  }

  @override
  Future<List<TodoCard>> getTodoByStatus(bool status) async{
     var maps = await DB.sharedInstance.queryAllRows();

     var list = List<TodoCard>.generate(maps.length, (i) {
      return TodoCard(
        id:        maps[i]['id'],
        title:        maps[i]['title'],
        description:  maps[i]['description'],
        created:      maps[i]['created'],
        updated:      maps[i]['updated'],
        status:       maps[i]['status'] == 1 ? true : false,
      );
    }).where((item) => item.status == status);

    return Future.value(list.toList());
  }

  @override
  Future<bool> updateTodo(Map<String, dynamic> todoCard, int id) async{
    try{
     final db = await DB.sharedInstance.database;
      await db .update(kTableTodos, todoCard, where: 'id=?', whereArgs: [id]);
      return Future.value(true);
    }
    catch(err) {
      return Future.value(false);
    }
    
    
  }
}