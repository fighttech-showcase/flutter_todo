import 'dart:convert';

import 'package:flutter_practical_todo/app/models/list_todo_card.dart';
import 'package:flutter_practical_todo/app/models/todo_card.dart';
import 'package:flutter_practical_todo/app/repository/rest_client_gen.dart';
import 'package:flutter_practical_todo/mocks/list_todo_cards.dart';

class RestMock implements RestClient{
  @override
  Future<ListTodoCards> getListTodoCards() async{
    return Future.value(ListTodoCards.fromJson(jsonDecode(mockListTodoCards)));
  }

  @override
  Future<bool> addTodoCard(Map<String, dynamic> todoCard) {
    
    return null;
  }

  @override
  Future<List<TodoCard>> getTodoByStatus(bool status) async{
     return null;
  }

  @override
  Future<bool> updateTodo(Map<String, dynamic> todoCard, int id) async{
    return  null;
  }
}