import 'package:dio/dio.dart';
import 'package:flutter_practical_todo/app/define.dart';
import 'package:flutter_practical_todo/app/repository/rest_client_gen.dart';
import 'package:flutter_practical_todo/app/repository/rest_client_local.dart';
import 'package:flutter_practical_todo/app/repository/rest_mock.dart';
import 'package:flutter_practical_todo/app/utils.dart';

class AppApiService {
  final _dio = Dio();
  RestClient client;
  ApiServiceHandler handlerEror;

  void create() {
    if(buildEnv == Env.MockOffline) {
      client = RestMock();
    } else if ( buildEnv == Env.LocalData) {
      client = RestClientLocal();
    }
    else {
      client = RestClient(_dio, baseUrl: appBaseUrl);
    }
    
    _dio.options.headers["X-Client-Id"] = "7E65424BD1DFF4CE17B77D74E4B96EB87137F6E1"; // config your _dio headers globally
    _dio.options.headers["Content-Type"] = "application/json";

    _dio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      printLog("[AppApiService][${DateTime.now().toString().split(' ').last}]-> _dioSTART\tonRequest \t${options.method} [${options.path}] ${options.contentType}");

      return options; //continue
    }, onResponse: (Response response) {
      printLog("[AppApiService][${DateTime.now().toString().split(' ').last}]-> _dioEND\tonResponse \t${response.statusCode} [${response.request.path}] ${response.request.method}  ${response.request.responseType}");

      return response; // continue
    }, onError: (DioError error) async {
      printLog("[AppApiService][${DateTime.now().toString().split(' ').last}]-> _dioEND\tonError \turl:[${error.request.baseUrl}] type:${error.type} message: ${error.message}");

       handlError(error);
    }));
  }

  dynamic handlError(DioError error) {
    if(handlerEror == null) {
      return null;
    }

    var result = ApiServerErrorData(
      type: ApiServerErrorType.UNKNOWN, 
      message: error.message
    );
    
    switch (error.type) {
      case DioErrorType.RECEIVE_TIMEOUT:
      case DioErrorType.SEND_TIMEOUT:
      case DioErrorType.CONNECT_TIMEOUT:
      result.type = ApiServerErrorType.TIMED_OUT;
        break;
      case DioErrorType.RESPONSE: {
        printLog("[AppApiService] _handleError DioErrorType.RESPONSE status code: ${error.response.statusCode}");
        result.statusCode = error.response.statusCode;
        
        if (result.statusCode == 401) {
          result.type = ApiServerErrorType.UNAUTHORIZED;
        } if (result.statusCode == 403) {
          //TODO: refresh token
        } else if (result.statusCode >= 500 && result.statusCode < 600) {
          result.type = ApiServerErrorType.HTTP_EXCEPTION;
        }
        else {
          result.type = ApiServerErrorType.HTTP_EXCEPTION;
          // result.message = getErrorMessage(error.response.data);
        }
        break;
      }
      case DioErrorType.CANCEL:
        break;
      case DioErrorType.DEFAULT:
        // TODO: Url not Server die or No Internet connection
        printLog("[AppApiService] _handleError DioErrorType.DEFAULT status code: error.response is null -> Server die or No Internet connection");
        result.type = ApiServerErrorType.NO_INTERNET;  
        
        if(error.message.contains('Unexpected character')) {
          result.type = ApiServerErrorType.SERVER_Unexpected_character;  
        }
        break;
    }

    return handlerEror.onError(result); //continue
  }

  String getErrorMessage(Map<String, dynamic> dataRes) {
    if (dataRes.containsKey("message") && dataRes["message"] != null) {
      return dataRes["message"]?.toString();
    }
    if (dataRes.containsKey("error") && dataRes["error"] != null) {
      return dataRes["error"]?.toString();
    }
    return dataRes.toString();
  }

}

enum ApiServerErrorType{
  NO_INTERNET,
  HTTP_EXCEPTION,
  TIMED_OUT,
  UNAUTHORIZED,
  UNKNOWN,
  SERVER_Unexpected_character
}

class ApiServerErrorData{ 
  ApiServerErrorType type;
  String message;
  int statusCode;

  ApiServerErrorData({this.type, this.statusCode, this.message});
}

abstract class ApiServiceHandler {
  onError(ApiServerErrorData onError);
}