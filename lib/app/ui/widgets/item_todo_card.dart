import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_practical_todo/app/models/todo_card.dart';
import 'package:flutter_practical_todo/app/screens/detail_todo.dart';
import 'package:flutter_practical_todo/app/values/styles.dart';

class ItemTodoCard extends StatefulWidget {
  const ItemTodoCard({
    this.index, 
    this.lastItem, 
    this.todoCard,
    this.onChanged
  });

  final int index;
  final bool lastItem;
  final TodoCard todoCard;
  final ValueChanged<bool> onChanged;

  @override
  _ItemTodoCardState createState() => _ItemTodoCardState();
}

class _ItemTodoCardState extends State<ItemTodoCard> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Widget row = GestureDetector(
      behavior: HitTestBehavior.opaque,
      onLongPress: () {
        Navigator.of(context).push(
          CupertinoPageRoute<void>(
            title: widget.todoCard?.title,
            builder: (BuildContext context) => DetaiTodo(
              index: widget.index,
            )
          )
        );
      },
      child: Padding(
        padding: const EdgeInsets.only(left: 16.0, top: 8.0, bottom: 8.0, right: 8.0),
        child: Row(
          children: <Widget>[
            Checkbox(
              value: widget.todoCard?.status,
              onChanged: (value) => widget.onChanged(value)
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.todoCard?.title, 
                      style:  AppStyleText.kCardTitle
                    ),
                    const Padding(
                      padding: EdgeInsets.only(top: 8.0)
                    ),
                    Text(
                      "#${widget.todoCard?.id} " + widget.todoCard?.description,
                      style: AppStyleText.kCardDescription
                    )
                  ]
                )
              )
            )
          ]
        )
      )
    );

    if (widget.lastItem) {
      return row;
    }

    return Column(
      children: <Widget>[
        row,
        Container(
          height: 1.0,
          color: const Color(0xFFD9D9D9),
        ),
      ],
    );
  }
}
