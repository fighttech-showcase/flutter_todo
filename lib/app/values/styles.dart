import 'package:flutter/material.dart';

const String kFontName = 'UTM';

class AppStyleText {
  static const kTitle = TextStyle(
    fontFamily: kFontName,
    color: Colors.white,
    fontWeight: FontWeight.bold,
    fontSize: 20.0
  );

  static const kCardTitle = TextStyle(
    fontSize: 15.0,
    fontFamily: kFontName
  );

  static const kCardDescription = TextStyle(
    color: Color(0xFF8E8E93),
    fontSize: 13.0,
    fontWeight: FontWeight.w300,
  );
}