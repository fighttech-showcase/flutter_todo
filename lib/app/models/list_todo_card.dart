import 'package:flutter_practical_todo/app/models/todo_card.dart';
import 'package:json_annotation/json_annotation.dart';

part 'list_todo_card.g.dart';

@JsonSerializable()
class ListTodoCards {
  @JsonKey(name: 'currentPage', includeIfNull: false)
  int currentPage;
  @JsonKey(name: 'items', includeIfNull: false)
  List<TodoCard> items;
  @JsonKey(name: 'totalItems', includeIfNull: false)
  int totalItems;

  ListTodoCards({
    this.currentPage,
    this.totalItems,
    this.items
  });

  factory ListTodoCards.fromJson(Map<String, dynamic> json) => _$ListTodoCardsFromJson(json);
  Map<String, dynamic> toJson() => _$ListTodoCardsToJson(this);

  @override
  String toString() {
    return toJson().toString();
  }
}