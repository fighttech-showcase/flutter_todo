// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_todo_card.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ListTodoCards _$ListTodoCardsFromJson(Map<String, dynamic> json) {
  return ListTodoCards(
    currentPage: json['currentPage'] as int,
    totalItems: json['totalItems'] as int,
    items: (json['items'] as List)
        ?.map((e) =>
            e == null ? null : TodoCard.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$ListTodoCardsToJson(ListTodoCards instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('currentPage', instance.currentPage);
  writeNotNull('items', instance.items);
  writeNotNull('totalItems', instance.totalItems);
  return val;
}
