// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'todo_card.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TodoCard _$TodoCardFromJson(Map<String, dynamic> json) {
  return TodoCard(
    title: json['title'] as String,
    description: json['description'] as String,
    created: json['created'] as String,
    updated: json['updated'] as String,
    status: json['status'] as bool,
    id: json['id'] as int,
  );
}

Map<String, dynamic> _$TodoCardToJson(TodoCard instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('title', instance.title);
  writeNotNull('description', instance.description);
  writeNotNull('created', instance.created);
  writeNotNull('updated', instance.updated);
  writeNotNull('status', instance.status);
  return val;
}
