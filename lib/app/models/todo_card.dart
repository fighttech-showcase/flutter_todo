import 'package:json_annotation/json_annotation.dart';

part 'todo_card.g.dart';

@JsonSerializable()
class TodoCard {
  @JsonKey(name: 'id', includeIfNull: false)
  int id;
  @JsonKey(name: 'title', includeIfNull: false)
  String title;
  @JsonKey(name: 'description', includeIfNull: false)
  String description;
   @JsonKey(name: 'created', includeIfNull: false)
  String created;
   @JsonKey(name: 'updated', includeIfNull: false)
  String updated;
  @JsonKey(name: 'status', includeIfNull: false)
  bool status;

  TodoCard({
    this.title,
    this.description,
    this.created,
    this.updated,
    this.status,
    this.id
  });
  
  factory TodoCard.fromJson(Map<String, dynamic> json) => _$TodoCardFromJson(json);
  Map<String, dynamic> toJson() => _$TodoCardToJson(this);
}