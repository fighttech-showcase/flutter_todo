import 'package:flutter/material.dart';
import 'package:flutter_practical_todo/app/define.dart';

printLog(dynamic data) {
  if(CHEAT == true) {
    print("[TodoApp]${data.toString()}");
  }
  
}

class Utils {
  static void hideKeyboard(BuildContext context) {
    printLog("[Utils] hideKeyboard");
    FocusScope.of(context).requestFocus(FocusNode());
  }
}

