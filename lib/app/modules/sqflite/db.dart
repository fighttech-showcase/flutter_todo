import 'dart:io';
import 'package:flutter_practical_todo/app/models/todo_card.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

const kTodosStatusActive = 0;
const kTodosStatusDone = 1;

const kDatabaseName = 'todos.db';
const kDatabaseVersion = 1;

const kTableTodos = 'todos';

const kSQLCreateStatement = '''

CREATE TABLE "$kTableTodos" (
  "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  "title" TEXT NOT NULL,
  "description" TEXT NOT NULL,
  "created" text ,
  "updated" TEXT ,
  "status" integer DEFAULT $kTodosStatusActive
);
''';


class DB {
  DB._();
  static final DB sharedInstance = DB._();

  Database _database;
  Future<Database> get database async {
    return _database ?? await initDB();
  }

  Future<Database> initDB() async {
    Directory docsDirectory = await getApplicationDocumentsDirectory();
    String path = join(docsDirectory.path, kDatabaseName);

    return await openDatabase(path, version: kDatabaseVersion,
        onCreate: (Database db, int version) async {
      await db.execute(kSQLCreateStatement);
    });
  }

  void createTodo(TodoCard todo) async {
    final db = await database;
    await db.insert(kTableTodos, todo.toJson());
  }

   Future<List<Map<String, dynamic>>> queryAllRows() async {
    Database db = await database;
    return await db.query(kTableTodos);
  }

  // void updateTodo(TodoCard todo) async {
  //   final db = await database;
  //   await db
  //       .update(kTableTodos, todo.toJson(), where: 'id=?', whereArgs: [todo.id]);
  // }

}