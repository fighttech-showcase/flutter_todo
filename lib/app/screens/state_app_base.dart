import 'package:flutter/material.dart';
import 'package:flutter_practical_todo/app/values/colors.dart';

abstract class StateBase<T extends StatefulWidget> extends State<T>{

  Widget buildLoading({double height}) {
    return Container(
      height: height,
      child: Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(AppColor.primaryColor)
        )
      ),
    );
  }
}