import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_practical_todo/app/bloc/dashboard/bloc_add_todo.dart';
import 'package:flutter_practical_todo/app/route/screen_navigator.dart';
import 'package:flutter_practical_todo/app/screens/dashboard/page_home.dart';
import 'package:flutter_practical_todo/app/utils.dart';
import 'package:flutter_practical_todo/app/values/colors.dart';
import 'package:flutter_practical_todo/app/values/styles.dart';
import 'package:toast/toast.dart';

class AddScreen extends StatefulWidget {
  NotifiChangeAdd listen;
  
  @override
  AddScreenState createState() {
    return AddScreenState();
  }
}

class AddScreenState extends State<AddScreen> {
  
  BlocAddTodo blocAddTodo = BlocAddTodo();

  @override
  void initState() {

    super.initState();
  }
  
  _onTapButtonAdd() {
    if(blocAddTodo.addTodo()) {      
      ScreenNavigator.toBack(context);
      widget.listen?.onAddSusscess();
    } else {
      Toast.show("Some thing wrong.. ", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.CENTER);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Utils.hideKeyboard(context),
      child: Scaffold(
        appBar: AppBar(
          title: Text('Add new todo'),
          // actions: <Widget>[
          //   FlatButton(
          //     child: Text(
          //       'XONG',
          //       style: AppStyleText.kTitle
          //     ),
          //     onPressed: () {
          //       Navigator.pop(context);
          //     },
          //   )
          // ],
        ),
        body: SafeArea(
          child: Stack(
            children: <Widget> [
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      key: Key('EDT_TITLE'),
                      controller: blocAddTodo.titleController,
                      decoration: InputDecoration(labelText: 'Enter title '),
                    )
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      key: Key('EDT_DESCRT'),
                      maxLines: 5,
                      controller: blocAddTodo.descriptionController,
                      decoration: InputDecoration(labelText: 'Enter content '),
                    )
                  ),
                ]
              ),
              
              Align(
                alignment: Alignment.bottomRight,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 10.0, right: 10.0),
                  child: CupertinoButton(
                    key: Key('BOTTON_ADD'),
                    color: AppColor.primaryColor,
                    child: Text(
                      "ADD",
                      style: AppStyleText.kTitle,
                    ),
                    onPressed: () => _onTapButtonAdd(),
                  ),
                )
              )
            ]
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    blocAddTodo.dispose();

    super.dispose();
  }
}
