import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_practical_todo/app/bloc/bloc_app.dart';

import 'package:flutter_practical_todo/app/route/screen_navigator.dart';
import 'package:flutter_practical_todo/app/screens/dashboard/page_complete.dart';
import 'package:flutter_practical_todo/app/screens/dashboard/page_home.dart';
import 'package:flutter_practical_todo/app/utils.dart';

class Dashboard extends StatefulWidget {
  final BlocApp blocApp;
  Dashboard({
    @required this.blocApp
  });

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  BlocApp get blocApp => widget.blocApp;

  @override
  void initState() {
    blocApp?.initialize();

    super.initState();
  }
  
  onPageComplete(bool status) {
    var screen = PageComplete(status: status);

    return screen;
  }

  @override
  Widget build(BuildContext context) {
    printLog('[Dashboard] BUILD');
    
    return GestureDetector(
      onHorizontalDragEnd: (onValue) {
        if(onValue.primaryVelocity > 900) {
          printLog("[AppMaster] open DEVMODE ${onValue.primaryVelocity}");
          ScreenNavigator.openDevMode(context);
        }
      },
      onTap: () => Utils.hideKeyboard(context),
      child: Scaffold(
        body: CupertinoTabScaffold(
            tabBar: CupertinoTabBar(
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(CupertinoIcons.home, key: Key('TAB_HOME')),
                  title: Text('Home'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.hourglass_full, key: Key('TAB_COMPLETE')), 
                  title: Text('Complete'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.hourglass_empty),
                  title: Text('In-Complete' , key: Key('TAB_INCOMPLETE')),
                )
              ]
            ),
            tabBuilder: (BuildContext context, int index) {
              switch (index) {
                case 0:
                  return CupertinoTabView(
                    builder: (BuildContext context) {
                      return PageHome();
                    },
                    defaultTitle: 'TodoApp',
                  );
                  break;
                case 1:
                  return CupertinoTabView(
                    builder: (BuildContext context) {
                      return onPageComplete(true);
                    },
                    defaultTitle: 'Complete',
                  );
                  break;
                case 2:
                  return CupertinoTabView(
                    builder: (BuildContext context) {
                      return onPageComplete(false);
                    },
                    defaultTitle: 'In-Complete',
                  );
                  break;
              }
              return null;
            }
          )
      )
    );
  }
}
