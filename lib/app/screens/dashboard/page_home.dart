
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_practical_todo/app/bloc/dashboard/bloc_home.dart';
import 'package:flutter_practical_todo/app/models/list_todo_card.dart';
import 'package:flutter_practical_todo/app/screens/add_screen.dart';
import 'package:flutter_practical_todo/app/screens/state_app_base.dart';
import 'package:flutter_practical_todo/app/ui/widgets/item_todo_card.dart';
import 'package:flutter_practical_todo/app/utils.dart';
import 'package:flutter_practical_todo/app/values/colors.dart';

abstract class NotifiChangeAdd {
  onAddSusscess();
}

class PageHome extends StatefulWidget {
  @override
  _PageHomeState createState() => _PageHomeState();
}

class _PageHomeState extends StateBase<PageHome> implements NotifiChangeAdd {
  BlocHome blocHome = BlocHome();

  @override
  void initState() {
    blocHome.initialize();
    
    super.initState();
  }

  @override
  void dispose() {
    blocHome.dispose();

    super.dispose();
  }

  @override
  onAddSusscess() {
    // TODO improve flow
    blocHome.initialize();
  }

  _onClickAddScreen() {
    printLog('[Dashboard] open AddScreen');
    // ScreenNavigator.openAddScreen(context);
    
    Navigator.push(
      context, 
      MaterialPageRoute(builder: (context) {
        var screen = AddScreen();
        screen.listen = this;
        return screen;
      })
    );
  }

  @override
  Widget build(BuildContext context) {
    printLog("[PageHome] BUILD ");

    return SafeArea(
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          key: Key('BOTTOM_ADD_TODO'),
          child: Icon(Icons.add),
          onPressed: () {
            _onClickAddScreen();
          },
        ),
        body: CustomScrollView(
          slivers: <Widget>[
            CupertinoSliverNavigationBar(
              trailing: Column(
                children: <Widget>[
                  Icon(
                    Icons.star,
                    size: 30.0,
                    color: AppColor.primaryColor
                  )
                ]
              )
            ),
            _buildListTodosCards()
          ]
        )
      ),
    );
  }
          
  _buildListTodosCards() {
    return StreamBuilder(
      stream: blocHome.controllerListTodosCard,
      builder: (BuildContext context, AsyncSnapshot<ListTodoCards> snapshot) {
        if (snapshot.hasData != null && snapshot.data != null && !snapshot.hasError) {
          return SliverPadding(
            padding: MediaQuery.of(context).removePadding(
              removeTop: true,
              removeLeft: true,
              removeRight: true
            ).padding,
            sliver: SliverList(
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return ItemTodoCard(
                    onChanged: (value) async{
                      printLog(value);
                      var result = await blocHome.onChaneItem(snapshot.data.items[index]);
                      if(result) {
                         setState(() {
                          snapshot.data.items[index].status = snapshot.data.items[index].status;
                        });
                      }
                    },
                    todoCard: snapshot.data.items[index],
                    index: index,
                    lastItem: index == snapshot.data.items.length - 1
                  );
                },
                childCount: snapshot.data.items.length
              )
            )
          );
        }
       
       return SliverFillRemaining (child: buildLoading());
      }
    );
  }
}