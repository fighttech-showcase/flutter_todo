import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_practical_todo/app/bloc/dashboard/bloc_list_todo.dart';
import 'package:flutter_practical_todo/app/models/todo_card.dart';
import 'package:flutter_practical_todo/app/screens/state_app_base.dart';
import 'package:flutter_practical_todo/app/utils.dart';
import 'package:flutter_practical_todo/app/values/styles.dart';

class PageComplete extends StatefulWidget {
  final bool status;
  PageComplete({this.status});

  @override
  _PageCompleteState createState() => _PageCompleteState();
}

class _PageCompleteState extends StateBase<PageComplete>{
  BlocListTodo blocListTodo = BlocListTodo();

  @override
  void initState() {
    
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    printLog("[PageComplete] BUILD ");
    blocListTodo.initialize(status: widget.status);

    return SafeArea(
      child: Scaffold(
        body: CustomScrollView(
          slivers: <Widget>[
            CupertinoSliverNavigationBar(
              trailing: Column(
                children: <Widget>[
                ]
              )
            ),
            _buildListTodosCards()
          ]
        )
      ),
    );    
  }

  _buildListTodosCards () {
    return StreamBuilder(
      stream: blocListTodo.controllerListTodosCard,
      builder: (BuildContext context, AsyncSnapshot<List<TodoCard>> snapshot) {
        if (snapshot.hasData != null && snapshot.data != null && !snapshot.hasError) {
          return SliverPadding(
            padding: MediaQuery.of(context).removePadding(
              removeTop: true,
              removeLeft: true,
              removeRight: true
            ).padding,
            sliver: SliverList(
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return _buildItem(snapshot.data[index], index);
                },
                childCount: snapshot.data.length
              )
            )
          );
        }

       return SliverFillRemaining (child: buildLoading());
      }
    );
  }

  _buildItem(TodoCard card, int index) {
    return Container(
      height: 70,
      color: index % 2 == 0 ? Colors.white : Color(0xFFD9D9D9),
      child: Padding(
        padding: const EdgeInsets.only(left: 16.0, top: 8.0, bottom: 8.0, right: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              card.title, 
              style:  AppStyleText.kCardTitle
            ),
            const Padding(
              padding: EdgeInsets.only(top: 8.0)
            ),
            Text(
              card.description,
              style: AppStyleText.kCardDescription
            )
          ]
        ),
      )
    );
  }
}