import 'package:flutter_practical_todo/app/repository/app_api_service.dart';

abstract class BlocBase {
  AppApiService appApiService = AppApiService();
  
  BlocBase() {
    appApiService.create();
  }
  
  initialize();
  dispose();
}