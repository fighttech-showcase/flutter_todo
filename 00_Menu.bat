REM Create by TrungHieuTran
REM Contact: hieu.trantrung1204@gmail.com
REM update submodule: git submodule update --init --recursive
@echo OFF
cls
set CUR_PATH=%~dp0

set FBUILD=%~dp0\Tools\
set FBUILD_CONFIG_WIN32=%~dp0\Tools\BuildWin32\fbuild.bff
set FBUILD_CONFIG_NATIVE=%~dp0\Tools\BuildNative\fbuild.bff

set Prj_WIN32=%~dp0\\Prj\Win32 
set Prj_ANDROID=%~dp0\\Prj\Android 

color 02
echo.
echo 	===================================
echo 	===============MENU================
echo 	===================================
echo.
echo 	  Working ON: %CUR_PATH%
echo.

echo 		01. flutter run 
echo 		02. RELEASE Android
echo 		03. RELEASE iOS
echo 		04. HIT GIT 
echo 		05. HIT Flutter 
echo 		06. Flutter Test: Drive Test 
echo 		07. Flutter MainDEV
echo 		08. Flutter Gen


echo.
set /p opt=		Enter choose: 

if "%opt%"=="1" goto Flutter_run
if "%opt%"=="2" goto Relase_android
if "%opt%"=="3" goto Relase_iOS
if "%opt%"=="4" goto Hit_Git
if "%opt%"=="5" goto Hit_flutter
if "%opt%"=="6" goto Flutter_test
if "%opt%"=="7" goto Flutter_maindev
if "%opt%"=="8" goto Flutter_gen

REM------------ 01. Option 1 Picked: flutter run .-------------
:Flutter_run
	call flutter run -d all
	exit /b
goto :End
REM-----------------------------------------------------------

REM------------ 02. Option 2 Picked: RELEASE Android"-------------
:Relase_android
	flutter build apk 
	adb install -r build/app/outputs/apk/release/app-release.apk
	exit /b 
goto :End
REM-----------------------------------------------------------

REM------------ 04. Option 4 Picked: HIT GIT-------------
:Hit_Git
	git fetch 
	git pull 
	git submodule update --init --recursive
	exit /b
goto :End
REM-----------------------------------------------------------

REM------------ 05. Option 5 Picked: HIT Flutter-------------
:Hit_flutter
	flutter upgrade 
	exit /b
goto :End
REM-----------------------------------------------------------

REM------------ 06. Option 6 Picked: FlutterTest: Drive Test -------------
:Flutter_test
	flutter drive --target=test_driver/app.dart
	exit /b
goto :End
REM-----------------------------------------------------------

REM------------ 07. Option 7 Picked: Flutter MainDEV -------------
:Flutter_maindev
	flutter run -t .\lib\main_dev.dart
	exit /b
goto :End
REM-----------------------------------------------------------

REM------------ 08. Option 8 Picked: Flutter gen -------------
:Flutter_gen
	flutter packages pub run build_runner build --delete-conflicting-outputs
	exit /b
goto :End
REM-----------------------------------------------------------


:End

pause

cd %CUR_PATH%
call 00_Make_Menu.bat

