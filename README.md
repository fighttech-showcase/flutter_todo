# flutter_practical_todo

+ Support Dark Mode 
+ Support CI/CD 


## Architecture
    - bloc (stream)
    - gen models & gen api (Retrofit)
    - sqlite
       
# Development 
    - Gen models, api
    ```
        flutter packages pub run build_runner build --delete-conflicting-outputs
    ```
# Run Test 
    Unit test: 
        flutter test
    Driver Test: connect device or emulator
        flutter drive --target=test_driver/app.dart