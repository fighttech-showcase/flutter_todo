#!/bin/sh
show_menu(){
    normal=`echo "\033[m"`
    menu=`echo "\033[36m"` #Blue
    number=`echo "\033[33m"` #yellow
    bgred=`echo "\033[41m"`
    fgred=`echo "\033[31m"`
    printf "\n${menu}*********************************************${normal}\n"
    printf "${menu}**${number} 1)${menu} Run ${normal}\n"
    printf "${menu}**${number} 2)${menu} RELEASE Android ${normal}\n"
    printf "${menu}**${number} 3)${menu} RELEASE iOS ${normal}\n"
    printf "${menu}**${number} 4)${menu} HIT GIT ${normal}\n"
    printf "${menu}**${number} 5)${menu} HIT Flutter ${normal}\n"
    printf "${menu}**${number} 6)${menu} Flutter test: Drive Test ${normal}\n"
    printf "${menu}**${number} 7)${menu} Flutter MainDEV ${normal}\n"
    printf "${menu}**${number} 8)${menu} Run mockServer ${normal}\n"
    printf "${menu}*********************************************${normal}\n"
    printf "Please enter a menu option and enter or ${fgred}x to exit. ${normal}"
    read opt
}

option_picked(){
    msgcolor=`echo "\033[01;31m"` # bold red
    normal=`echo "\033[00;00m"` # normal white
    message=${@:-"${normal}Error: No message passed"}
    printf "${msgcolor}${message}${normal}\n"
}

clear
show_menu
while [ $opt != '' ]
    do
    if [ $opt = '' ]; then
      exit;
    else
      case $opt in
        1) clear;
            option_picked "Option 1 Picked: flutter run ";
            flutter run -d all 
            show_menu;
        ;;
        2) clear;
            option_picked "Option 2 Picked: RELEASE Android";
            flutter build apk 
            adb install -r build/app/outputs/apk/release/app-release.apk 
            show_menu;
        ;;
        3) clear;
            option_picked "Option 3 Picked: RELEASE iOS";
            flutter build ios
            show_menu;
        ;;
        4) clear;
            option_picked "Option 4 Picked: HIT GIT ";
            git fetch 
            git pull
            git submodule update --init --recursive 
            show_menu;
        ;;
        5) clear;
            option_picked "Option 5 Picked: HIT Flutter ";
            flutter upgrade  
            show_menu;
        ;;
        6) clear;
            option_picked "Option 6 Picked: FlutterTest: Drive Test ";
            flutter drive --target=test_driver/app.dart 
            show_menu;
        ;;
        7) clear;
            option_picked "Option 7 Picked: Run MainDev";
            flutter run -t lib/main_dev.dart -d all
            show_menu;
        ;;
        8) clear;
            option_picked "Option 8 Picked: Run mockServer";
            cd mockServer
            npm i 
            npm start
            show_menu;
        ;;
        \n)exit;
        ;;
        *)clear;
            option_picked "Pick an option from the menu";
            show_menu;
        ;;
      esac
    fi

printf " DONE ..\n";
done