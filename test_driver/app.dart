
import 'package:flutter_driver/driver_extension.dart';
import 'package:flutter_practical_todo/main.dart' as app;

void main () {
  enableFlutterDriverExtension();
  app.main();
}