import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group('TODO APP', () {
      FlutterDriver driver;

    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });
    test('Automation Test', () async {
      Health health = await driver.checkHealth();
      if(health.status == HealthStatus.ok) {
        final btnAdd = find.byValueKey('BOTTOM_ADD_TODO');
        await driver.tap(btnAdd);

        await Future.delayed(Duration(seconds: 1));
        final edtTitle = find.byValueKey('EDT_TITLE');
        await driver.tap(edtTitle);
        await driver.enterText('title 1');

        final edtDescrt = find.byValueKey('EDT_DESCRT');
        await driver.tap(edtDescrt);
        await driver.enterText('hihi 1');
        final btnAddTodo = find.byValueKey('BOTTON_ADD');
        await driver.tap(btnAddTodo);

        await Future.delayed(Duration(seconds: 2));
        final tab1 = find.byValueKey('TAB_COMPLETE');
        await driver.tap(tab1);

        await Future.delayed(Duration(seconds: 1));
        final tab2 = find.byValueKey('TAB_INCOMPLETE');
        await driver.tap(tab2);

        await Future.delayed(Duration(seconds: 1));
        final tab3 = find.byValueKey('TAB_HOME');
        await driver.tap(tab3);


        driver.close();
      }
    });
  });
}