import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:flutter_practical_todo/app/screens/dashboard/dashboard.dart';
import 'package:flutter_practical_todo/app/bloc/bloc_app.dart';

import 'package:flutter_practical_todo/app/screens/dashboard/page_home.dart';
import 'package:flutter_practical_todo/app/bloc/dashboard/bloc_home.dart';

void main() {
  testWidgets('Unselected item is shown as unselected', (WidgetTester tester) async {
    // Build Screen Dashboaed
    await tester.pumpWidget(DashboardTest());
    await tester.pump(Duration(seconds: 1));

    //Check widget loading in home page
    var findLoading = find.byType(CircularProgressIndicator);
    await tester.pump(Duration(seconds: 1));
    expect(findLoading, findsWidgets);

    //Check button bottom add todo and tap in button
    final btnAddTodo = find.byKey(Key("BOTTOM_ADD_TODO"));
    await tester.tap(btnAddTodo);
    await tester.pump(Duration(seconds: 2));
    
    // After tap in button Bottom add todo => push to screen add and check avaliable widget loading in screen?
    findLoading = find.byType(CircularProgressIndicator);
    await tester.pump(Duration(seconds: 1));
    expect(findLoading, findsNothing);

    // enter text to titile and todo 
    final textTitle = find.byKey(Key('EDT_TITLE'));
    await tester.pump(Duration(seconds: 2));
    
    await tester.enterText(textTitle, "hello");
    await tester.pump(Duration(seconds: 1));

    final textTodo = find.byKey(Key('EDT_DESCRT'));
    await tester.pump(Duration(seconds: 1));

    await tester.enterText(textTodo, "hello 2");
    await tester.pump(Duration(seconds: 1));

    // find and tap in button ADD
    final btnAddInScreenAdd = find.byKey(Key("BOTTON_ADD"));
    await tester.pump(Duration(seconds: 2));

    expect(btnAddInScreenAdd, findsWidgets);

    await tester.press(btnAddInScreenAdd);
    await tester.pump(Duration(seconds: 1));   

    // Find todo if the string "hello" exists on the screen?
    var findTodoTitleInHome = find.text("hello");
    await tester.pump(Duration(seconds: 1));   

    
    expect(textTitle, findsWidgets);
    expect(textTodo, findsWidgets);
    expect(findTodoTitleInHome, findsWidgets);

  });
}


class DashboardTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Dashboard(blocApp: BlocApp())
    );
  }
}
