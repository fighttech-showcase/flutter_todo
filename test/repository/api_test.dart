import 'package:flutter_practical_todo/app/define.dart';
import 'package:test/test.dart';

import 'package:flutter_practical_todo/app/models/list_todo_card.dart';
import 'package:flutter_practical_todo/app/repository/app_api_service.dart';
import 'package:flutter_practical_todo/app/utils.dart';

main() {
   group('Test Networking', () {
     test('Test API MockOffline', () async{
       buildEnv = Env.MockOffline;

        AppApiService apiService = AppApiService()
        ..create();

        ListTodoCards cards = await apiService.client.getListTodoCards();
        expect(true, cards != null);
        printLog("[TestAPI] ${cards.toJson()}");

     });
     
   });
}